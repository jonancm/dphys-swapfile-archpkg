# dphys-swapfile package for Arch Linux

This repository contains all the files needed to create a package of [dphys-swapfile][7] for the [Arch User Repository][5], so that you can install and use dphys-swapfile on Arch Linux.

dphys-swapfile is a Bash shell script to create, manage and use swap files instead of swap partitions, which has several advantages.

_Note: In the following, we will denote a root shell with the prompt '#', and a non-root shell with the prompt '$'. This indicates what commands need to be run as a privileged user, either in a root shell or by using [sudo][10], if you have [set it up][10]._

## How to create the package

Enter the repository root directory (where the PKGBUILD file is located) and run the following command (as a non-privileged user!):

	$ makepkg

This will create the package with a name of the form `dphys-swapfile-$pkgver-$pkgrel-any.pkg.tar.xz` and make a link to the package in the current directory. For example, for `pkgver=20061020` and `pkgrel=1`, the created file will be `dphys-swapfile-20061020-1-any.pkg.tar.xz`.

## How to install the package on your system

You can now use [pacman][6] (or [yaourt][9], if you prefer) to install the package on your system:

	# pacman -U dphys-swapfile-20061020-1-any.pkg.tar.xz

or, alternatively,

	$ yaourt -U dphys-swapfile-20061020-1-any.pkg.tar.xz

Remember that pacman needs to be run as root, while yaourt will automatically detect when it needs root privilege and ask for your password ([sudo][10] needs to be set up for your user for this to work).

## Usage

### Configuration

This package installs the configuration file to `/etc/dphys-swapfile`. This is just a shell script with the variable assignments needed to configure dphys-swapfile.

After installing this package, have a look at this configuration file to see if you need to change it before running dphys-swapfile. The variables that you can specify here are:

- `CONF_SWAPFILE`: path to the swap file (default: `/var/swap`)
- `CONF_SWAPSIZE`: desired swap file size in bytes (default: `SWAPFACTOR * RAM`)
- `SWAPFACTOR`: factor by which the RAM size should be multiplied to get the value of `CONF_SWAPSIZE`, if it has not been specified explicitly (default: `2`)
- `MAXSWAP`: max size limit (in MB) for the swap file, which will never be exceeded, even if `CONF_SWAPSIZE` is greater than this (default: `2048`)

_Note: Whenever you change the configuration file, you will have to restart dphys-swapfile for the changes to take effect. This is done manually with `dphys-swapfile swapoff` followed by `dphys-swapfile swapon`, or with `systemctl` as described below._

### Manual usage

The dphys-swapfile script can be run manually to set up, enable and disable the use of swap files. The usefulness of this is, however, very limited, since in most cases you will want the system to take care of swap file creation automatically on system startup.

To get started, you need to set up your system to use swap files and create the swap files:

	# dphys-swapfile setup

After this command, you can now enable the use of swap files:

	# dphys-swapfile swapon

If you want to check whether the use swap files is now enabled, simply run:

	$ free

### Usage as a service

A `dphys-swapfile.service` file is provided with this package, which is installed when you install the package, and makes systemd aware of dphys-swapfile.

Thus, if you want to run dphys-swapfile as a daemon, you can simply run:

	# systemctl start dphys-swapfile

Then, you will be able to stop it with:

	# systemctl stop dphys-swapfile

This will not start dphys-swapfile on startup, though. To do this, run:

	# systemctl enable dphys-swapfile

Finally, if you want to prevent dphys-swapfile from starting automatically on boot, run:

	# systemctl disable dphys-swapfile

_Note: Installing this package will by default enable the dphys-swapfile service, as this is the desired behaviour in most cases. If you do not want this for whatever reason, simply disable the service after installing this package._

## References

- [Arch Build System][4]
- [Arch User Repository][5]
- [Creating packages][1]
- [PKGBUILD][2]
- [makepkg][3]
- [pacman][6]

[1]: https://wiki.archlinux.org/index.php/Creating_packages
[2]: https://wiki.archlinux.org/index.php/PKGBUILD
[3]: https://wiki.archlinux.org/index.php/makepkg
[4]: https://wiki.archlinux.org/index.php/Arch_Build_System
[5]: https://wiki.archlinux.org/index.php/Arch_User_Repository
[6]: https://wiki.archlinux.org/index.php/Pacman
[7]: https://github.com/isgphys/dphys-swapfile
[8]: http://neil.franklin.ch/Projects/dphys-swapfile/
[9]: https://wiki.archlinux.org/index.php/yaourt
[10]: https://wiki.archlinux.org/index.php/sudo
